var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from 'express';
import AWS from 'aws-sdk';
const app = express();
const PORT = 8001;
app.listen(PORT, () => {
    console.log('server is running at port 8001');
});
AWS.config.update({
    region: "us-west-2",
    endpoint: "http://localhost:8000"
    // accesskeyid: "ys",
    // secretaccesskey: "ys",
});
const dynamodb = new AWS.DynamoDB();
app.get('/', (req, res) => {
    res.send('Access the List of tables from server');
});
app.get('/createTable', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const tablename = "todo_app";
    const listMyTables = () => __awaiter(void 0, void 0, void 0, function* () {
        const tables = (yield dynamodb.listTables());
        console.log(tables.TableNames);
        const tablenames = tables.TableNames ? tables.TableNames : [];
        console.log("Listing table names " + tablenames);
        for (let i = 0; i < tablenames.length; i++) {
            console.log("console log is working");
            if (tablenames[i] == tablename) {
                console.log("createTable Done");
                return;
            }
        }
        Create_table(); //To be done
    });
    var params = {
        TableName: "todo_app",
        KeySchema: [
            { AttributeName: "username", KeyType: "HASH" },
        ],
        AttributeDefinitions: [
            { AttributeName: "username", AttributeType: "S" },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 5,
            WriteCapacityUnits: 5
        }
    };
    const Create_table = () => {
        console.log("table is being created........");
        dynamodb.createTable(params, function (err, data) {
            if (err) {
                console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
            }
            else {
                console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
            }
        });
    };
    listMyTables();
    console.log("done");
    res.send("A response is received");
}));
//# sourceMappingURL=create_table.js.map