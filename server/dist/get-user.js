var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from 'express';
import bodyparser from 'body-parser';
import AWS from 'aws-sdk';
import cors from 'cors';
const port = 8001;
const app = express();
app.listen(port, () => {
    console.log("The server is running on loaclhost 8001");
});
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(cors());
AWS.config.update({
    region: "us-west-2",
    endpoint: "http://localhost:8000"
});
var docClient = new AWS.DynamoDB.DocumentClient();
app.get('/', (req, res) => {
    res.send("Yeah the server is running");
});
//req.body.userName
app.post('/getUser', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var user = req.body.username;
    var table = "todo_app";
    var task = " ";
    const CreateUser = () => {
        var params2 = {
            TableName: table,
            Item: {
                "username": user,
                "todo": task,
                "stat": 1
            }
        };
        console.log("Adding a new user...");
        docClient.put(params2, function (err, data) {
            if (err) {
                console.error("Unable to add user. Error JSON:", JSON.stringify(err, null, 2));
            }
            else {
                console.log("Added user in your database:", JSON.stringify(data, null, 2));
            }
        });
    };
    var params = {
        TableName: table,
        Key: {
            "username": user,
        }
    };
    docClient.get(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        }
        else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            if (typeof data.Item !== "undefined") {
                res.send("User already exists");
            }
            else {
                CreateUser();
                res.send("Creating user " + req.body.username);
                // res.json({
                //     "name":"done"
                // })
                // CreateUser();
                return;
            }
        }
    });
}));
//# sourceMappingURL=get-user.js.map