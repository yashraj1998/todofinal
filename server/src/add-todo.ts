import express from 'express'
import bodyparser from 'body-parser';
import AWS from 'aws-sdk';


const port = 8001;
const app = express();
app.listen(port, () => {
    console.log("The server is running on loaclhost 8001");
});


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));


AWS.config.update({
    region: "us-west-2",
    //endpoint: "http://localhost:8000"
});

var docClient = new AWS.DynamoDB.DocumentClient();

app.get('/', (req, res) => {
    res.send("Yeah the server is running");
})

var table = "todo_app_list";
app.put('/AddTodo', async (req, res) => {
    var user = req.query.username;
    var task = req.query.todo;

    var params = {
        TableName: table,
        Item: {
            "username": user,
            "todo": task,
            "stat": 0,
        },
    };
    console.log("Updating the todo list...");
    docClient.put(params, function (err: any, data: any) {
        if (err) {
            console.error("Unable to add task. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Add task completed:", JSON.stringify(data, null, 2));
        }
    });
    res.send("Adding Todo ......");
});





