import express, { Request, Response } from 'express'
import bodyparsor from 'body-parser';
import AWS from 'aws-sdk'
import { ListTablesOutput } from 'aws-sdk/clients/dynamodb';

const app = express();
const PORT = 8001;

app.listen(PORT, () => {
    console.log('server is running at port 8001');
});


AWS.config.update({
    region: "us-west-2",
    endpoint: "http://localhost:8000"
    // accesskeyid: "ys",
    // secretaccesskey: "ys",
});
const dynamodb = new AWS.DynamoDB();

app.get('/', (req: Request, res: Response) => {
    res.send('Access the List of tables from server')
});


app.get('/createTable', async (req: Request, res: Response) => {

    const tablename = "todo_app";
    const listMyTables = async () => {

        const tables = (await dynamodb.listTables()) as ListTablesOutput;
        console.log(tables.TableNames);
        const tablenames = tables.TableNames ? tables.TableNames : [];

        console.log("Listing table names " + tablenames);

        for (let i = 0; i < tablenames.length; i++) {
            console.log("console log is working");
            if (tablenames[i] == tablename) {
                console.log("createTable Done");
                return;
            }
        }
        Create_table();//To be done
    }

    var params = {
        TableName: "todo_app",
        KeySchema: [
            { AttributeName: "username", KeyType: "HASH" },
            // { AttributeName: "todo", KeyType: "SORT" },
        ],
        AttributeDefinitions: [
            { AttributeName: "username", AttributeType: "S" },
            // { AttributeName: "todo", AttributeType: "S" },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 5,
            WriteCapacityUnits: 5
        }
    };

    const Create_table = () => {
        console.log("table is being created........");
        dynamodb.createTable(params, function (err: any, data: any) {
            if (err) {
                console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
            }
        });
    }
    listMyTables();

    console.log("done");
    res.send("A response is received");
})