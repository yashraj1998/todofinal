import * as React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Helmet } from "react-helmet-async";
import Dropdown from "react-bootstrap/Dropdown";
import ListGroup from "react-bootstrap/ListGroup";
import { Jumbotron } from "react-bootstrap";
import { Button } from "react-bootstrap";
function message12() {
  console.log("To-do added");
}

interface TodoList {
  toDoItem: string;
}
export class Add extends React.Component<any, TodoList> {
  state = {
    toDoItem: "",
  };
  setUser = (e) => {
    this.setState({
      toDoItem: e.target.value,
    });
  };
  display = () => {
    alert("adding new todo in your database");
    var body = {
      task: `${this.state.toDoItem}`,
    };
    axios
      .post(`http://localhost:8002/AddTodo?username=yash`, body)
      .then((response) => {
        alert(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <>
        <Helmet>
          <title>Add Todo</title>
          <meta
            name="description"
            content="A Boilerplate application homepage"
          />
        </Helmet>
        <span>
          <div className="jumbotron">
            <h1>Add To-Do</h1>

            <input
              onChange={this.setUser}
              type="text"
              placeholder="Enter todo here"
              value={this.state.toDoItem}
            ></input>
            <div>
              <Button onClick={this.display}>ADD</Button>
              <Link to="To" className="btn btn-primary btn-md">
                CANCEL
              </Link>
            </div>
          </div>
        </span>
      </>
    );
  }
}
