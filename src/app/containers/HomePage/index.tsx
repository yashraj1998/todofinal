import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as action from '../../../locales/types';
import axios from 'axios';
interface UserItem {
  userName: string;
}

export class HomePage extends React.Component<any, UserItem> {
  state = {
    userName: '',
  };
  setUser = e => {
    this.setState({
      userName: e.target.value,
    });
  };
  // display = () => {
  //   alert('Hello ' + this.state.userName);
  //   // this.props.dispatch({
  //   //   type: action.ADD_USER_REQUESTED
  //   // })
  // };
  display = () => {
    // alert('Hello ' + this.state.userName);
    var body = {
      "username": `${this.state.userName}`,
    }
    axios.post('http://localhost:8001/getUser', body)
      .then(response => {
        alert(response.data);
      })
      .catch(error => {
        console.log(error);
      })
  }
  render() {
    return (
      <>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A Boilerplate application homepage"
          />
        </Helmet>
        <span>
          <div className="jumbotron">
            <h1>To-DO</h1>

            <input
              onChange={this.setUser}
              type="text"
              placeholder="User Name"
              value={this.state.userName}
              required
            ></input>
            <Link
              to="To"
              className="btn btn-primary btn-lg"
              onClick={this.display}
            >
              Submit
            </Link>
          </div>
        </span>
      </>
    );
  }
}

export default connect(
  null,
  null
)(HomePage);