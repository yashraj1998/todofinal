import * as React from "react";
import { Helmet } from "react-helmet-async";
import Dropdown from "react-bootstrap/Dropdown";
import ListGroup from "react-bootstrap/ListGroup";
import { Jumbotron, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Color } from "chalk";

function StatusUpdate() {
  alert("Toggle Status");
}

export class To extends React.Component {
  render() {
    return (
      <>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A Boilerplate application homepage"
          />
        </Helmet>
        <span>
          <div>
            <div>
              <Container>
                <h2>To-do List</h2>
                <div>
                  <Dropdown>
                    ----------
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      All Task{" "}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item href="https://app.pluralsight.com/library/courses/react-js-getting-started/table-of-contents/action-1">
                        Complete
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Incomplete
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                <ListGroup>
                  <ListGroup.Item>
                    Complete to-do{" "}
                    <button className="list" onClick={StatusUpdate}>
                      incom{" "}
                    </button>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    Bring Grocceries{" "}
                    <button onClick={StatusUpdate}>incom </button>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    Complete Plural Sight{" "}
                    <button onClick={StatusUpdate}> incom </button>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    Complete Dark{" "}
                    <button onClick={StatusUpdate}> incom </button>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    Vestibulum at eros{" "}
                    <button onClick={StatusUpdate}> incom </button>
                  </ListGroup.Item>
                </ListGroup>
                <button className="btn btn-primary btn-lg">Submit</button>
                ____________
                <Link to="Add" className="btn btn-primary btn-lg">
                  {" "}
                  ADD{" "}
                </Link>
              </Container>
            </div>
          </div>
        </span>
      </>
    );
  }
}
