import { lazyLoad } from 'utils/loadable';

export const To = lazyLoad(
  () => import('./index'),
  module => module.To,
);
