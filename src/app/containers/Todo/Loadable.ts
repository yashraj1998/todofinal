/**
 *
 * Asynchronously loads the component for Todo
 *
 */

import { lazyLoad } from 'utils/loadable';

export const Todo = lazyLoad(
  () => import('./index'),
  module => module.Todo,
);
