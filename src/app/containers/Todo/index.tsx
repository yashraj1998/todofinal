/**
 *
 * Todo
 *
 */

import React, { memo } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey } from './slice';
import { selectTodo } from './selectors';
import { todoSaga } from './saga';
import { messages } from './messages';

interface Props {}

export const Todo = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: todoSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const todo = useSelector(selectTodo);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <>
      <Helmet>
        <title>Todo</title>
        <meta name="description" content="Description of Todo" />
      </Helmet>
      <Div>
        {t('')}
        {/*  {t(...messages.someThing)}  */}
      </Div>
    </>
  );
});

const Div = styled.div``;
