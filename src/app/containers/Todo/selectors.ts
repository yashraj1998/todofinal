import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.todo || initialState;

export const selectTodo = createSelector(
  [selectDomain],
  todoState => todoState,
);
