import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the Todo container
export const initialState: ContainerState = {};

const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    someAction(state, action: PayloadAction<any>) {},
  },
});

export const { actions: todoActions, reducer, name: sliceKey } = todoSlice;
